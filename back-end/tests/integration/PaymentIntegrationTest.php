<?php

declare(strict_types=1);

namespace Tests\Integration;

use Challenge\Domain\User\Entity\Address;
use Challenge\Domain\User\Entity\Payment;
use Challenge\Domain\User\Entity\User;
use Tests\TestCase;

final class PaymentIntegrationTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        self::$conn->prepare('DELETE FROM user')->execute();
        self::$conn->prepare('ALTER TABLE user AUTO_INCREMENT=1;')->execute();
    }

    /**
     * @covers \Challenge\Infrastructure\Http\Controller\Controller::jsonResponse
     * @covers \Challenge\Infrastructure\Http\Controller\PaymentController::create
     * @covers \Challenge\Application\Service\AddressService::create
     * @covers \Challenge\Application\Service\PaymentExternalService::checkResponse
     * @covers \Challenge\Application\Service\PaymentExternalService::getPayload
     * @covers \Challenge\Application\Service\PaymentExternalService::pay
     * @covers \Challenge\Application\Service\PaymentService::create
     * @covers \Challenge\Application\Service\UserService::create
     * @covers \Challenge\Application\Service\UserService::getById
     * @covers \Challenge\Domain\User\Entity\Address::fill
     * @covers \Challenge\Domain\User\Entity\Address::getCity
     * @covers \Challenge\Domain\User\Entity\Address::getHouseNumber
     * @covers \Challenge\Domain\User\Entity\Address::getStreet
     * @covers \Challenge\Domain\User\Entity\Address::getUserId
     * @covers \Challenge\Domain\User\Entity\Address::getZipCode
     * @covers \Challenge\Domain\User\Entity\Address::jsonSerialize
     * @covers \Challenge\Domain\User\Entity\Address::setCity
     * @covers \Challenge\Domain\User\Entity\Address::setHouseNumber
     * @covers \Challenge\Domain\User\Entity\Address::setStreet
     * @covers \Challenge\Domain\User\Entity\Address::setUserId
     * @covers \Challenge\Domain\User\Entity\Address::setZipCode
     * @covers \Challenge\Domain\User\Entity\Payment::fill
     * @covers \Challenge\Domain\User\Entity\Payment::getAccountOwner
     * @covers \Challenge\Domain\User\Entity\Payment::getIban
     * @covers \Challenge\Domain\User\Entity\Payment::getPaymentDataId
     * @covers \Challenge\Domain\User\Entity\Payment::getUserId
     * @covers \Challenge\Domain\User\Entity\Payment::jsonSerialize
     * @covers \Challenge\Domain\User\Entity\Payment::setAccountOwner
     * @covers \Challenge\Domain\User\Entity\Payment::setIban
     * @covers \Challenge\Domain\User\Entity\Payment::setPaymentDataId
     * @covers \Challenge\Domain\User\Entity\Payment::setUserId
     * @covers \Challenge\Domain\User\Entity\User::fill
     * @covers \Challenge\Domain\User\Entity\User::getFirstName
     * @covers \Challenge\Domain\User\Entity\User::getId
     * @covers \Challenge\Domain\User\Entity\User::getLastName
     * @covers \Challenge\Domain\User\Entity\User::getTelephone
     * @covers \Challenge\Domain\User\Entity\User::jsonSerialize
     * @covers \Challenge\Domain\User\Entity\User::setFirstName
     * @covers \Challenge\Domain\User\Entity\User::setId
     * @covers \Challenge\Domain\User\Entity\User::setLastName
     * @covers \Challenge\Domain\User\Entity\User::setTelephone
     * @covers \Challenge\Infrastructure\Http\Controller\AddressController::create
     * @covers \Challenge\Infrastructure\Http\Controller\Controller::getPathVariable
     * @covers \Challenge\Infrastructure\Http\Controller\UserController::create
     * @covers \Challenge\Infrastructure\Repository\User\AddressRepository::create
     * @covers \Challenge\Infrastructure\Repository\User\PaymentRepository::create
     * @covers \Challenge\Infrastructure\Repository\User\UserRepository::create
     * @covers \Challenge\Infrastructure\Repository\User\UserRepository::getById
     */
    public function testShouldCreateAPayment(): void
    {
        $user = new User();
        $user->fill((object) ['id' => 1, 'firstName' => 'Klay', 'lastName' => 'Thompson', 'telephone' => '234-567-365']);

        $request = $this->createJsonRequest('POST', '/users', ['firstName' => 'Klay', 'lastName' => 'Thompson', 'telephone' => '234-567-365']);
        $response = $this->app->handle($request);

        $jsonDecodeResponse = json_decode((string) $response->getBody());

        $this->assertSame(self::HTTP_STATUS_201, $response->getStatusCode());

        $address = new Address();
        $address->fill((object) ['userId' => 1, 'street' => 'Some Street', 'houseNumber' => '8547', 'zipCode' => '254-897', 'city' => 'San Francisco']);

        $request = $this->createJsonRequest('POST', "/users/{$user->getId()}/address", ['street' => 'Some Street', 'houseNumber' => '8547', 'zipCode' => '254-897', 'city' => 'San Francisco']);
        $response = $this->app->handle($request);

        $jsonDecodeResponse = json_decode((string) $response->getBody());

        $this->assertSame(self::HTTP_STATUS_201, $response->getStatusCode());
        $this->assertJson((string) $response->getBody());
        $this->assertEquals('User address successfully registered!', $jsonDecodeResponse->message);
        $this->assertEquals($address->getUserId(), $jsonDecodeResponse->data->userId);
        $this->assertEquals($address->getStreet(), $jsonDecodeResponse->data->street);
        $this->assertEquals($address->getHouseNumber(), $jsonDecodeResponse->data->houseNumber);
        $this->assertEquals($address->getZipCode(), $jsonDecodeResponse->data->zipCode);
        $this->assertEquals($address->getCity(), $jsonDecodeResponse->data->city);

        $payment = new Payment();
        $payment->fill((object) ['userId' => $user->getId(), 'accountOwner' => 'John Terry', 'iban' => 'DE8547AB']);

        $request = $this->createJsonRequest('POST', "/users/{$user->getId()}/payment", ['accountOwner' => 'John Terry', 'iban' => 'DE8547AB']);
        $response = $this->app->handle($request);

        $jsonDecodeResponse = json_decode((string) $response->getBody());

        $this->assertSame(self::HTTP_STATUS_201, $response->getStatusCode());
        $this->assertJson((string) $response->getBody());
        $this->assertEquals('Payment successfully made!', $jsonDecodeResponse->message);
        $this->assertEquals($payment->getUserId(), $jsonDecodeResponse->data->userId);
        $this->assertEquals($payment->getAccountOwner(), $jsonDecodeResponse->data->accountOwner);
        $this->assertEquals($payment->getIban(), $jsonDecodeResponse->data->iban);
        $this->assertNotEmpty($jsonDecodeResponse->data->paymentDataId);
    }

    /**
     * @covers \Challenge\Infrastructure\Http\Controller\Controller::jsonResponse
     * @covers \Challenge\Infrastructure\Http\Controller\PaymentController::create
     * @covers \Challenge\Application\Service\UserService::getById
     * @covers \Challenge\Infrastructure\Http\Controller\Controller::getPathVariable
     * @covers \Challenge\Infrastructure\Http\ErrorHandler::handle
     * @covers \Challenge\Infrastructure\Repository\User\UserRepository::getById
     */
    public function testShouldNotCreateAPaymentWhenUserDoesNotExists(): void
    {
        $request = $this->createJsonRequest('POST', "/users/0/payment", ['accountOwner' => 'Draymon Green', 'iban' => 'UT87OP']);
        $response = $this->app->handle($request);

        $jsonDecodeResponse = json_decode((string) $response->getBody());
        $this->assertSame(self::HTTP_STATUS_404, $response->getStatusCode());
        $this->assertJson((string) $response->getBody());
        $this->assertEquals('User not found.', $jsonDecodeResponse->errors->message);
    }

    /**
     * @covers \Challenge\Infrastructure\Http\Controller\Controller::jsonResponse
     * @covers \Challenge\Infrastructure\Http\Controller\PaymentController::create
     * @covers \Challenge\Application\Service\AddressService::create
     * @covers \Challenge\Application\Service\PaymentService::create
     * @covers \Challenge\Application\Service\UserService::create
     * @covers \Challenge\Application\Service\UserService::getById
     * @covers \Challenge\Domain\User\Entity\Address::fill
     * @covers \Challenge\Domain\User\Entity\Address::getCity
     * @covers \Challenge\Domain\User\Entity\Address::getHouseNumber
     * @covers \Challenge\Domain\User\Entity\Address::getStreet
     * @covers \Challenge\Domain\User\Entity\Address::getUserId
     * @covers \Challenge\Domain\User\Entity\Address::getZipCode
     * @covers \Challenge\Domain\User\Entity\Address::jsonSerialize
     * @covers \Challenge\Domain\User\Entity\Address::setCity
     * @covers \Challenge\Domain\User\Entity\Address::setHouseNumber
     * @covers \Challenge\Domain\User\Entity\Address::setStreet
     * @covers \Challenge\Domain\User\Entity\Address::setUserId
     * @covers \Challenge\Domain\User\Entity\Address::setZipCode
     * @covers \Challenge\Domain\User\Entity\Payment::fill
     * @covers \Challenge\Domain\User\Entity\Payment::setAccountOwner
     * @covers \Challenge\Domain\User\Entity\Payment::setIban
     * @covers \Challenge\Domain\User\Entity\Payment::setPaymentDataId
     * @covers \Challenge\Domain\User\Entity\Payment::setUserId
     * @covers \Challenge\Domain\User\Entity\User::fill
     * @covers \Challenge\Domain\User\Entity\User::getFirstName
     * @covers \Challenge\Domain\User\Entity\User::getId
     * @covers \Challenge\Domain\User\Entity\User::getLastName
     * @covers \Challenge\Domain\User\Entity\User::getTelephone
     * @covers \Challenge\Domain\User\Entity\User::jsonSerialize
     * @covers \Challenge\Domain\User\Entity\User::setFirstName
     * @covers \Challenge\Domain\User\Entity\User::setId
     * @covers \Challenge\Domain\User\Entity\User::setLastName
     * @covers \Challenge\Domain\User\Entity\User::setTelephone
     * @covers \Challenge\Infrastructure\Http\Controller\AddressController::create
     * @covers \Challenge\Infrastructure\Http\Controller\Controller::getPathVariable
     * @covers \Challenge\Infrastructure\Http\Controller\UserController::create
     * @covers \Challenge\Infrastructure\Http\ErrorHandler::handle
     * @covers \Challenge\Infrastructure\Repository\User\AddressRepository::create
     * @covers \Challenge\Infrastructure\Repository\User\UserRepository::create
     * @covers \Challenge\Infrastructure\Repository\User\UserRepository::getById
     */
    public function testShouldNotCreateAPaymentWhenPaymentHasEmptyFields(): void
    {
        $user = new User();
        $user->fill((object) ['id' => 1, 'firstName' => 'Klay', 'lastName' => 'Thompson', 'telephone' => '234-567-365']);

        $request = $this->createJsonRequest('POST', '/users', ['firstName' => 'Klay', 'lastName' => 'Thompson', 'telephone' => '234-567-365']);
        $response = $this->app->handle($request);

        $jsonDecodeResponse = json_decode((string) $response->getBody());

        $this->assertSame(self::HTTP_STATUS_201, $response->getStatusCode());

        $address = new Address();
        $address->fill((object) ['userId' => 1, 'street' => 'Some Street', 'houseNumber' => '8547', 'zipCode' => '254-897', 'city' => 'San Francisco']);

        $request = $this->createJsonRequest('POST', "/users/{$user->getId()}/address", ['street' => 'Some Street', 'houseNumber' => '8547', 'zipCode' => '254-897', 'city' => 'San Francisco']);
        $response = $this->app->handle($request);

        $jsonDecodeResponse = json_decode((string) $response->getBody());

        $this->assertSame(self::HTTP_STATUS_201, $response->getStatusCode());
        $this->assertJson((string) $response->getBody());
        $this->assertEquals('User address successfully registered!', $jsonDecodeResponse->message);
        $this->assertEquals($address->getUserId(), $jsonDecodeResponse->data->userId);
        $this->assertEquals($address->getStreet(), $jsonDecodeResponse->data->street);
        $this->assertEquals($address->getHouseNumber(), $jsonDecodeResponse->data->houseNumber);
        $this->assertEquals($address->getZipCode(), $jsonDecodeResponse->data->zipCode);
        $this->assertEquals($address->getCity(), $jsonDecodeResponse->data->city);

        $payment = new Payment();
        $payment->fill((object) ['userId' => $user->getId(), 'accountOwner' => 'John Terry', 'iban' => 'DE8547AB']);

        $request = $this->createJsonRequest('POST', "/users/{$user->getId()}/payment", ['iban' => '']);
        $response = $this->app->handle($request);

        $jsonDecodeResponse = json_decode((string) $response->getBody());

        $this->assertSame(self::HTTP_STATUS_422, $response->getStatusCode());
        $this->assertJson((string) $response->getBody());
        $this->assertEquals('The account owner cannot be empty!', $jsonDecodeResponse->errors->message);
    }
}
