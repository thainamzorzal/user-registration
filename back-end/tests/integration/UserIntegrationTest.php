<?php

declare(strict_types=1);

namespace Tests\Integration;

use Challenge\Domain\User\Entity\User;
use Tests\TestCase;

final class UserIntegrationTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        self::$conn->prepare('DELETE FROM user')->execute();
        self::$conn->prepare('ALTER TABLE user AUTO_INCREMENT=1;')->execute();
    }
    /**
     * @covers \Challenge\Infrastructure\Http\Controller\Controller::jsonResponse
     * @covers \Challenge\Infrastructure\Http\Controller\UserController
     * @covers \Challenge\Application\Service\UserService::create
     * @covers \Challenge\Domain\User\Entity\User::fill
     * @covers \Challenge\Domain\User\Entity\User::getFirstName
     * @covers \Challenge\Domain\User\Entity\User::getId
     * @covers \Challenge\Domain\User\Entity\User::getLastName
     * @covers \Challenge\Domain\User\Entity\User::getTelephone
     * @covers \Challenge\Domain\User\Entity\User::jsonSerialize
     * @covers \Challenge\Domain\User\Entity\User::setFirstName
     * @covers \Challenge\Domain\User\Entity\User::setId
     * @covers \Challenge\Domain\User\Entity\User::setLastName
     * @covers \Challenge\Domain\User\Entity\User::setTelephone
     * @covers \Challenge\Infrastructure\Repository\User\UserRepository::create
     */
    public function testShouldCreateAnUser(): void
    {
        $user = new User();
        $user->fill((object) ['id' => 1, 'firstName' => 'Steph', 'lastName' => 'Curry', 'telephone' => '658-254-897']);

        $request = $this->createJsonRequest('POST', '/users', ['firstName' => 'Steph', 'lastName' => 'Curry', 'telephone' => '658-254-897']);
        $response = $this->app->handle($request);

        $jsonDecodeResponse = json_decode((string) $response->getBody());

        $this->assertSame(self::HTTP_STATUS_201, $response->getStatusCode());
        $this->assertJson((string) $response->getBody());
        $this->assertEquals('User successfully registered!', $jsonDecodeResponse->message);
        $this->assertEquals($user->getId(), $jsonDecodeResponse->data->id);
        $this->assertEquals($user->getFirstName(), $jsonDecodeResponse->data->firstName);
        $this->assertEquals($user->getLastName(), $jsonDecodeResponse->data->lastName);
        $this->assertEquals($user->getTelephone(), $jsonDecodeResponse->data->telephone);
    }

    /**
     * @covers \Challenge\Infrastructure\Http\Controller\Controller::jsonResponse
     * @covers \Challenge\Infrastructure\Http\Controller\UserController
     * @covers \Challenge\Application\Service\UserService::create
     * @covers \Challenge\Domain\User\Entity\User::fill
     * @covers \Challenge\Domain\User\Entity\User::setFirstName
     * @covers \Challenge\Infrastructure\Http\ErrorHandler::handle
     */
    public function testShouldNotCreateAnUserMissingInformation(): void
    {

        $request = $this->createJsonRequest('POST', '/users', ['firstName' => '']);
        $response = $this->app->handle($request);

        $jsonDecodeResponse = json_decode((string) $response->getBody());

        $this->assertSame(self::HTTP_STATUS_422, $response->getStatusCode());
        $this->assertSame(self::HTTP_STATUS_422, $jsonDecodeResponse->errors->code);
        $this->assertSame('The first name cannot be empty!', $jsonDecodeResponse->errors->message);
    }

    /**
     * @covers \Challenge\Infrastructure\Http\Controller\Controller::jsonResponse
     * @covers \Challenge\Infrastructure\Http\Controller\UserController::checkUser
     * @covers \Challenge\Application\Service\UserService::create
     * @covers \Challenge\Application\Service\UserService::getById
     * @covers \Challenge\Domain\User\Entity\User::fill
     * @covers \Challenge\Domain\User\Entity\User::getFirstName
     * @covers \Challenge\Domain\User\Entity\User::getId
     * @covers \Challenge\Domain\User\Entity\User::getLastName
     * @covers \Challenge\Domain\User\Entity\User::getTelephone
     * @covers \Challenge\Domain\User\Entity\User::jsonSerialize
     * @covers \Challenge\Domain\User\Entity\User::setFirstName
     * @covers \Challenge\Domain\User\Entity\User::setId
     * @covers \Challenge\Domain\User\Entity\User::setLastName
     * @covers \Challenge\Domain\User\Entity\User::setTelephone
     * @covers \Challenge\Infrastructure\Http\Controller\Controller::getPathVariable
     * @covers \Challenge\Infrastructure\Http\Controller\UserController::create
     * @covers \Challenge\Infrastructure\Repository\User\UserRepository::create
     * @covers \Challenge\Infrastructure\Repository\User\UserRepository::getById
     */
    public function testShouldReturnAnUserWhenExists(): void
    {
        $userInfo = ['firstName' => 'Klay', 'lastName' => 'Thompson', 'telephone' => '234-567-365'];
        $user = new User();
        $user->fill((object) $userInfo);

        $request = $this->createJsonRequest('POST', '/users', $userInfo);
        $response = $this->app->handle($request);

        $jsonDecodeResponse = json_decode((string) $response->getBody());

        $this->assertSame(self::HTTP_STATUS_201, $response->getStatusCode());

        $user->setId($jsonDecodeResponse->data->id);

        $request = $this->createJsonRequest('GET', "/users/{$user->getId()}");
        $response = $this->app->handle($request);

        $jsonDecodeResponse = json_decode((string) $response->getBody());

        $this->assertSame(self::HTTP_STATUS_200, $response->getStatusCode());
        $this->assertJson((string) $response->getBody());
        $this->assertEquals($user->getId(), $jsonDecodeResponse->data->id);
        $this->assertEquals($user->getFirstName(), $jsonDecodeResponse->data->firstName);
        $this->assertEquals($user->getLastName(), $jsonDecodeResponse->data->lastName);
        $this->assertEquals($user->getTelephone(), $jsonDecodeResponse->data->telephone);
    }

    /**
     * @covers \Challenge\Infrastructure\Http\Controller\Controller::jsonResponse
     * @covers \Challenge\Infrastructure\Http\Controller\UserController::checkUser
     * @covers \Challenge\Application\Service\UserService::getById
     * @covers \Challenge\Infrastructure\Http\Controller\Controller::getPathVariable
     * @covers \Challenge\Infrastructure\Http\ErrorHandler::handle
     * @covers \Challenge\Infrastructure\Repository\User\UserRepository::getById
     */
    public function testShouldNotReturnAnUserIfDoesntExists(): void
    {
        $request = $this->createJsonRequest('GET', "/users/0");
        $response = $this->app->handle($request);

        $jsonDecodeResponse = json_decode((string) $response->getBody());

        $this->assertSame(self::HTTP_STATUS_404, $response->getStatusCode());
        $this->assertJson((string) $response->getBody());
        $this->assertEquals('User not found.', $jsonDecodeResponse->errors->message);
    }
}
