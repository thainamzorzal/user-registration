<?php

declare(strict_types=1);

namespace Tests\Unit;

use Challenge\Domain\User\Entity\Address;
use stdClass;
use Tests\TestCase;

final class AddressTest extends TestCase
{
    /**
     * @covers \Challenge\Domain\User\Entity\Address::setUserId
     * @covers \Challenge\Domain\User\Entity\Address::getUserId
     */
    public function testShouldPopulateUserIdWhenNotEmpty(): void
    {
        $userId = 1;
        $address = new Address();
        $address->setUserId($userId);

        $this->assertEquals(
            $userId,
            $address->getUserId()
        );
    }

    /**
     * @covers \Challenge\Domain\User\Entity\Address::setUserId
     * @covers \Challenge\Domain\User\Entity\Address::getUserId
     */
    public function testShouldNotPopulateUserIdWhenNotValid(): void
    {
        $userId = 0;
        $address = new Address();

        $this->expectException('InvalidArgumentException');
        $address->setUserId($userId);
    }

    /**
     * @covers \Challenge\Domain\User\Entity\Address::setStreet
     * @covers \Challenge\Domain\User\Entity\Address::getStreet
     */
    public function testShouldPopulateAddressStreetWhenNotEmpty(): void
    {
        $street = 'My Street';
        $address = new Address();
        $address->setStreet($street);

        $this->assertEquals(
            $street,
            $address->getStreet()
        );
    }

    /**
     * @covers \Challenge\Domain\User\Entity\Address::setStreet
     * @covers \Challenge\Domain\User\Entity\Address::getStreet
     */
    public function testShouldNotPopulateAddressStreetWhenEmpty(): void
    {
        $street = '';
        $address = new Address();

        $this->expectException('InvalidArgumentException');
        $address->setStreet($street);
    }

    /**
     * @covers \Challenge\Domain\User\Entity\Address::setHouseNumber
     * @covers \Challenge\Domain\User\Entity\Address::getHouseNumber
     */
    public function testShouldPopulateAddressHouseNumberWhenIsNotEmpty(): void
    {
        $houseNumber = '4005';
        $address = new Address();
        $address->setHouseNumber($houseNumber);

        $this->assertEquals(
            $houseNumber,
            $address->getHouseNumber()
        );
    }

   /**
     * @covers \Challenge\Domain\User\Entity\Address::setHouseNumber
     * @covers \Challenge\Domain\User\Entity\Address::getHouseNumber
     */
    public function testShouldNotPopulateAddressHouseNumberWhenIsEmpty(): void
    {
        $houseNumber = '';
        $address = new Address();
        $this->expectException('InvalidArgumentException');
        $address->sethouseNumber($houseNumber);
    }

    /**
     * @covers \Challenge\Domain\User\Entity\Address::setZipCode
     * @covers \Challenge\Domain\User\Entity\Address::getZipCode
     */
    public function testShouldNotPopulateAddressZipCodeWhenIsNotEmpty(): void
    {
        $zipCode = '2265-987';
        $address = new Address();
        $address->setZipCode($zipCode);

        $this->assertEquals(
            $zipCode,
            $address->getZipCode()
        );
    }

    /**
     * @covers \Challenge\Domain\User\Entity\Address::setZipCode
     * @covers \Challenge\Domain\User\Entity\Address::getZipCode
     */
    public function testShouldNotPopulateAddressZipCodeIsEmpty(): void
    {
        $zipCode = '';
        $address = new Address();

        $this->expectException('InvalidArgumentException');
        $address->setZipCode($zipCode);
    }

    /**
     * @covers \Challenge\Domain\User\Entity\Address::setCity
     * @covers \Challenge\Domain\User\Entity\Address::getCity
     */
    public function testShouldNotPopulateAddressCityWhenIsNotEmpty(): void
    {
        $city = 'Cariacica';
        $address = new Address();
        $address->setCity($city);

        $this->assertEquals(
            $city,
            $address->getCity()
        );
    }


    /**
     * @covers \Challenge\Domain\User\Entity\Address::setCity
     * @covers \Challenge\Domain\User\Entity\Address::getCity
     */
    public function testShouldNotPopulateAddressCityIsEmpty(): void
    {
        $city = '';
        $address = new Address();

        $this->expectException('InvalidArgumentException');
        $address->setCity($city);
    }

    /**
     * @covers \Challenge\Domain\User\Entity\Address::fill
     * @covers \Challenge\Domain\User\Entity\Address::setUserId
     * @covers \Challenge\Domain\User\Entity\Address::setStreet
     * @covers \Challenge\Domain\User\Entity\Address::setHouseNumber
     * @covers \Challenge\Domain\User\Entity\Address::setZipCode
     * @covers \Challenge\Domain\User\Entity\Address::setCity
     * @covers \Challenge\Domain\User\Entity\Address::getUserId
     * @covers \Challenge\Domain\User\Entity\Address::getStreet
     * @covers \Challenge\Domain\User\Entity\Address::getHouseNumber
     * @covers \Challenge\Domain\User\Entity\Address::getZipCode
     * @covers \Challenge\Domain\User\Entity\Address::getCity
     */
    public function testShouldPopulateAllAddressInfo(): void
    {
        $addressInfo = new stdClass();
        $addressInfo->userId = 10;
        $addressInfo->street = 'My Street';
        $addressInfo->houseNumber = '5689';
        $addressInfo->zipCode = '654-987';
        $addressInfo->city = 'Cariacica';

        $address = new Address();
        $address->fill($addressInfo);

        $this->assertEquals($addressInfo->userId, $address->getUserId());
        $this->assertEquals($addressInfo->street, $address->getStreet());
        $this->assertEquals($addressInfo->houseNumber, $address->getHouseNumber());
        $this->assertEquals($addressInfo->zipCode, $address->getZipCode());
        $this->assertEquals($addressInfo->city, $address->getCity());
    }


    /**
     * @covers \Challenge\Domain\User\Entity\Address::fill
     * @covers \Challenge\Domain\User\Entity\Address::setUserId
     * @covers \Challenge\Domain\User\Entity\Address::setStreet
     * @covers \Challenge\Domain\User\Entity\Address::setHouseNumber
     * @covers \Challenge\Domain\User\Entity\Address::setZipCode
     * @covers \Challenge\Domain\User\Entity\Address::setCity
     * @covers \Challenge\Domain\User\Entity\Address::getUserId
     * @covers \Challenge\Domain\User\Entity\Address::getStreet
     * @covers \Challenge\Domain\User\Entity\Address::getHouseNumber
     * @covers \Challenge\Domain\User\Entity\Address::getZipCode
     * @covers \Challenge\Domain\User\Entity\Address::getCity
     * @covers \Challenge\Domain\User\Entity\Address::jsonSerialize
     */
    public function testShouldSerializeAddressInfo(): void
    {
        $addressInfo = new stdClass();
        $addressInfo->userId = 10;
        $addressInfo->street = 'My Street';
        $addressInfo->houseNumber = '5689';
        $addressInfo->zipCode = '654-987';
        $addressInfo->city = 'Cariacica';

        $address = new Address();
        $address->fill($addressInfo);
        $addressSerialized = json_encode($address);

        $expected = '{"userId":10,"street":"My Street","houseNumber":"5689","zipCode":"654-987","city":"Cariacica"}';

        $this->assertEquals($expected, $addressSerialized);
    }
}
