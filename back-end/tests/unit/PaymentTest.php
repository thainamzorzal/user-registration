<?php

declare(strict_types=1);

namespace Tests\Payment;

use Challenge\Domain\User\Entity\Payment;
use stdClass;
use Tests\TestCase;

final class PaymentTest extends TestCase
{
    /**
     * @covers \Challenge\Domain\User\Entity\Payment::setUserId
     * @covers \Challenge\Domain\User\Entity\Payment::getUserId
     */
    public function testShouldPopulateUserIdWhenNotEmpty(): void
    {
        $id = 1;
        $payment = new Payment();
        $payment->setUserId($id);

        $this->assertEquals(
            $id,
            $payment->getUserId()
        );
    }

    /**
     * @covers \Challenge\Domain\User\Entity\Payment::setUserId
     * @covers \Challenge\Domain\User\Entity\Payment::getUserId
     */
    public function testShouldNotPopulateUserIdWhenNotValid(): void
    {
        $id = 0;
        $payment = new Payment();

        $this->expectException('InvalidArgumentException');
        $payment->setUserId($id);
    }

    /**
     * @covers \Challenge\Domain\User\Entity\Payment::setAccountOwner
     * @covers \Challenge\Domain\User\Entity\Payment::getAccountOwner
     */
    public function testShouldPopulateAccountOwnerWhenNotEmpty(): void
    {
        $accountOwner = 'John Terry';
        $payment = new Payment();
        $payment->setAccountOwner($accountOwner);

        $this->assertEquals(
            $accountOwner,
            $payment->getAccountOwner()
        );
    }

    /**
     * @covers \Challenge\Domain\User\Entity\Payment::setAccountOwner
     * @covers \Challenge\Domain\User\Entity\Payment::getAccountOwner
     */
    public function testShouldNotPopulateAccountOwnerWhenEmpty(): void
    {
        $accountOwner = '';
        $payment = new Payment();

        $this->expectException('InvalidArgumentException');
        $payment->setAccountOwner($accountOwner);
    }

    /**
     * @covers \Challenge\Domain\User\Entity\Payment::setIban
     * @covers \Challenge\Domain\User\Entity\Payment::getIban()

     */
    public function testShouldPopulateIbanWhenIsNotEmpty(): void
    {
        $iban = 'DH87UY';
        $payment = new Payment();
        $payment->setIban($iban);

        $this->assertEquals(
            $iban,
            $payment->getIban()
        );
    }

    /**
     * @covers \Challenge\Domain\User\Entity\Payment::setIban
     * @covers \Challenge\Domain\User\Entity\Payment::getIban()
     */
    public function testShouldNotPopulateIbanWhenIsEmpty(): void
    {
        $iban = '';
        $payment = new Payment();
        $this->expectException('InvalidArgumentException');
        $payment->setIban($iban);
    }

    /**
     * @covers \Challenge\Domain\User\Entity\Payment::setPaymentDataId
     * @covers \Challenge\Domain\User\Entity\Payment::getPaymentDataId
     */
    public function testShouldPopulatePaymentDataIdWhenIsNotEmpty(): void
    {
        $paymentDataId = 'c68644153714ca78e4102c7f5';
        $payment = new Payment();
        $payment->setPaymentDataId($paymentDataId);

        $this->assertEquals(
            $paymentDataId,
            $payment->getPaymentDataId()
        );
    }

    /**
     * @covers \Challenge\Domain\User\Entity\Payment::setPaymentDataId
     * @covers \Challenge\Domain\User\Entity\Payment::getPaymentDataId
     */
    public function testShouldPopulatePaymentDataIdWhenIsNull(): void
    {
        $paymentDataId = null;
        $payment = new Payment();
        $payment->setPaymentDataId($paymentDataId);

        $this->assertNull($payment->getPaymentDataId());
    }

    /**
     * @covers \Challenge\Domain\User\Entity\Payment::fill
     * @covers \Challenge\Domain\User\Entity\Payment::setUserId
     * @covers \Challenge\Domain\User\Entity\Payment::getUserId
     * @covers \Challenge\Domain\User\Entity\Payment::setAccountOwner
     * @covers \Challenge\Domain\User\Entity\Payment::getAccountOwner
     * @covers \Challenge\Domain\User\Entity\Payment::setIban
     * @covers \Challenge\Domain\User\Entity\Payment::getIban
     * @covers \Challenge\Domain\User\Entity\Payment::setPaymentDataId
     * @covers \Challenge\Domain\User\Entity\Payment::getPaymentDataId
     */
    public function testShouldPopulateAllPaymentInfo(): void
    {
        $paymentInfo = new stdClass();
        $paymentInfo->userId = 25;
        $paymentInfo->accountOwner = 'John Terry';
        $paymentInfo->iban = 'DH87UY';
        $paymentInfo->paymentDataId = 'c68644153714ca78e4102c7f5';

        $payment = new Payment();
        $payment->fill($paymentInfo);

        $this->assertEquals($paymentInfo->userId, $payment->getUserId());
        $this->assertEquals($paymentInfo->accountOwner, $payment->getAccountOwner());
        $this->assertEquals($paymentInfo->iban, $payment->getIban());
        $this->assertEquals($paymentInfo->paymentDataId, $payment->getPaymentDataId());
    }

    /**
     * @covers \Challenge\Domain\User\Entity\Payment::fill
     * @covers \Challenge\Domain\User\Entity\Payment::setUserId
     * @covers \Challenge\Domain\User\Entity\Payment::getUserId
     * @covers \Challenge\Domain\User\Entity\Payment::setAccountOwner
     * @covers \Challenge\Domain\User\Entity\Payment::getAccountOwner
     * @covers \Challenge\Domain\User\Entity\Payment::setIban
     * @covers \Challenge\Domain\User\Entity\Payment::getIban
     * @covers \Challenge\Domain\User\Entity\Payment::setPaymentDataId
     * @covers \Challenge\Domain\User\Entity\Payment::getPaymentDataId
     * @covers \Challenge\Domain\User\Entity\Payment::jsonSerialize
     */
    public function testShouldSerializePaymentInfo(): void
    {
        $paymentInfo = new stdClass();
        $paymentInfo->userId = 25;
        $paymentInfo->accountOwner = 'John Terry';
        $paymentInfo->iban = 'DH87UY';
        $paymentInfo->paymentDataId = 'c68644153714ca78e4102c7f5';

        $payment = new Payment();
        $payment->fill($paymentInfo);
        $paymentSerialized = json_encode($payment);

        $expected = '{"userId":25,"accountOwner":"John Terry","iban":"DH87UY","paymentDataId":"c68644153714ca78e4102c7f5"}';

        $this->assertEquals($expected, $paymentSerialized);
    }
}
