<?php

declare(strict_types=1);

namespace Tests\Unit;

use Challenge\Domain\User\Entity\User;
use stdClass;
use Tests\TestCase;

final class UserTest extends TestCase
{
    /**
     * @covers \Challenge\Domain\User\Entity\User::setId
     * @covers \Challenge\Domain\User\Entity\User::getId
     */
    public function testShouldPopulateUserIdWhenNotEmpty(): void
    {
        $id = 1;
        $user = new User();
        $user->setId($id);

        $this->assertEquals(
            $id,
            $user->getId()
        );
    }

    /**
     * @covers \Challenge\Domain\User\Entity\User::setId
     * @covers \Challenge\Domain\User\Entity\User::getId
     */
    public function testShouldNotPopulateUserIdWhenNotValid(): void
    {
        $id = 0;
        $user = new User();

        $this->expectException('InvalidArgumentException');
        $user->setId($id);
    }

    /**
     * @covers \Challenge\Domain\User\Entity\User::setFirstName
     * @covers \Challenge\Domain\User\Entity\User::getFirstName
     */
    public function testShouldPopulateUserFirstNameWhenNotEmpty(): void
    {
        $firstName = 'John';
        $user = new User();
        $user->setFirstName($firstName);

        $this->assertEquals(
            $firstName,
            $user->getFirstName()
        );
    }

    /**
     * @covers \Challenge\Domain\User\Entity\User::setFirstName
     * @covers \Challenge\Domain\User\Entity\User::getFirstName
     */
    public function testShouldNotPopulateUserFirstNameWhenEmpty(): void
    {
        $firstName = '';
        $user = new User();

        $this->expectException('InvalidArgumentException');
        $user->setFirstName($firstName);
    }

    /**
     * @covers \Challenge\Domain\User\Entity\User::setLastName
     * @covers \Challenge\Domain\User\Entity\User::getLastName
     */
    public function testShouldPopulateUserLastNameWhenIsNotEmpty(): void
    {
        $lastName = 'Terry';
        $user = new User();
        $user->setLastName($lastName);

        $this->assertEquals(
            $lastName,
            $user->getLastName()
        );
    }

   /**
     * @covers \Challenge\Domain\User\Entity\User::setLastName
     * @covers \Challenge\Domain\User\Entity\User::getLastName
     */
    public function testShouldNotPopulateUserLastNameWhenIsEmpty(): void
    {
        $lastName = '';
        $user = new User();
        $this->expectException('InvalidArgumentException');
        $user->setLastName($lastName);
    }

    /**
     * @covers \Challenge\Domain\User\Entity\User::setTelephone
     * @covers \Challenge\Domain\User\Entity\User::getTelephone
     */
    public function testShouldNotPopulateUserTelephoneWhenIsNotEmpty(): void
    {
        $telephone = '(542) 552-258';
        $user = new User();
        $user->setTelephone($telephone);

        $this->assertEquals(
            $telephone,
            $user->getTelephone()
        );
    }


    /**
     * @covers \Challenge\Domain\User\Entity\User::setTelephone
     * @covers \Challenge\Domain\User\Entity\User::getTelephone
     */
    public function testShouldNotPopulateUserTelephoneIsEmpty(): void
    {
        $telephone = '';
        $user = new User();

        $this->expectException('InvalidArgumentException');
        $user->setTelephone($telephone);
    }

    /**
     * @covers \Challenge\Domain\User\Entity\User::fill
     * @covers \Challenge\Domain\User\Entity\User::setId
     * @covers \Challenge\Domain\User\Entity\User::setFirstName
     * @covers \Challenge\Domain\User\Entity\User::setLastName
     * @covers \Challenge\Domain\User\Entity\User::setTelephone
     * @covers \Challenge\Domain\User\Entity\User::getId
     * @covers \Challenge\Domain\User\Entity\User::getFirstName
     * @covers \Challenge\Domain\User\Entity\User::getLastName
     * @covers \Challenge\Domain\User\Entity\User::getTelephone
     */
    public function testShouldPopulateAllUserInfo(): void
    {
        $userInfo = new stdClass();
        $userInfo->id = 1;
        $userInfo->firstName = 'John';
        $userInfo->lastName = 'Terry';
        $userInfo->telephone = '(542) 552-258';

        $user = new User();
        $user->fill($userInfo);

        $this->assertEquals($userInfo->id, $user->getId());
        $this->assertEquals($userInfo->firstName, $user->getFirstName());
        $this->assertEquals($userInfo->lastName, $user->getLastName());
        $this->assertEquals($userInfo->telephone, $user->getTelephone());
    }

    /**
     * @covers \Challenge\Domain\User\Entity\User::fill
     * @covers \Challenge\Domain\User\Entity\User::setId
     * @covers \Challenge\Domain\User\Entity\User::setFirstName
     * @covers \Challenge\Domain\User\Entity\User::setLastName
     * @covers \Challenge\Domain\User\Entity\User::setTelephone
     * @covers \Challenge\Domain\User\Entity\User::getId
     * @covers \Challenge\Domain\User\Entity\User::getFirstName
     * @covers \Challenge\Domain\User\Entity\User::getLastName
     * @covers \Challenge\Domain\User\Entity\User::getTelephone
     * @covers \Challenge\Domain\User\Entity\User::jsonSerialize
     */
    public function testShouldSerializeAddressInfo(): void
    {
        $userInfo = new stdClass();
        $userInfo->id = 32;
        $userInfo->firstName = 'John';
        $userInfo->lastName = 'Terry';
        $userInfo->telephone = '(542) 552-258';

        $user = new User();
        $user->fill($userInfo);
        $userSerialized = json_encode($user);

        $expected = '{"id":32,"firstName":"John","lastName":"Terry","telephone":"(542) 552-258"}';

        $this->assertEquals($expected, $userSerialized);
    }
}
