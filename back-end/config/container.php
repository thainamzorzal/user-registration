<?php

use Challenge\Application\Service\AddressService;
use Challenge\Application\Service\AddressServiceInterface;
use Challenge\Application\Service\UserService;
use Challenge\Application\Service\PaymentExternalService;
use Challenge\Application\Service\PaymentExternalServiceInterface;
use Challenge\Application\Service\PaymentService;
use Challenge\Application\Service\PaymentServiceInterface;
use Challenge\Application\Service\UserServiceInterface;
use Challenge\Infrastructure\Repository\User\AddressRepository;
use Challenge\Infrastructure\Repository\User\PaymentRepository;
use Challenge\Infrastructure\Repository\User\UserRepository;
use GuzzleHttp\ClientInterface as GuzzleClientInterface;
use Psr\Container\ContainerInterface;
use Selective\BasePath\BasePathMiddleware;
use Slim\App;
use Slim\Factory\AppFactory;
use Slim\Middleware\ErrorMiddleware;

return [
    'settings' => function () {
        return require __DIR__ . '/settings.php';
    },

    App::class => function (ContainerInterface $container) {
        AppFactory::setContainer($container);

        return AppFactory::create();
    },

    ErrorMiddleware::class => function (ContainerInterface $container) {
        $app = $container->get(App::class);
        $settings = $container->get('settings')['error'];

        return new ErrorMiddleware(
            $app->getCallableResolver(),
            $app->getResponseFactory(),
            (bool)$settings['display_error_details'],
            (bool)$settings['log_errors'],
            (bool)$settings['log_error_details']
        );
    },

    PDO::class => function (ContainerInterface $container) {
        $settings = $container->get('settings')['db'];

        $host = $settings['host'];
        $dbname = $settings['database'];
        $username = $settings['username'];
        $password = $settings['password'];
        $charset = $settings['charset'];
        $flags = $settings['flags'];
        $dsn = "mysql:host=$host;dbname=$dbname;charset=$charset";

        return new PDO($dsn, $username, $password, $flags);
    },

    GuzzleClientInterface::class => function(ContainerInterface $container) {
        $externalPaymentHost = $container->get('settings')['externalPaymentHost'];
        return new GuzzleHttp\Client(['base_uri' => $externalPaymentHost]);
    },

    BasePathMiddleware::class => function (ContainerInterface $container) {
        return new BasePathMiddleware($container->get(App::class));
    },

    UserServiceInterface::class => function (ContainerInterface $container) {
        $pdo = $container->get(PDO::class);
        $userRepository = new UserRepository($pdo);
        return new UserService($userRepository);
    },

    AddressServiceInterface::class => function (ContainerInterface $container) {
        $pdo = $container->get(PDO::class);
        $addressRepository = new AddressRepository($pdo);
        return new AddressService($addressRepository);
    },

    PaymentExternalServiceInterface::class => function (ContainerInterface $container) {
        $externalPaymentHost = $container->get(GuzzleClientInterface::class);
        return new PaymentExternalService($externalPaymentHost);
    },

    PaymentServiceInterface::class => function (ContainerInterface $container) {
        $pdo = $container->get(PDO::class);
        $paymentRepository = new PaymentRepository($pdo);
        $externalPaymentService = $container->get(PaymentExternalServiceInterface::class);
        return new PaymentService($paymentRepository, $externalPaymentService);
    },
];
