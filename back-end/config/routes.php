<?php

use Challenge\Infrastructure\Http\Controller\AddressController;
use Challenge\Infrastructure\Http\Controller\PaymentController;
use Challenge\Infrastructure\Http\Controller\UserController;
use Slim\App;
use Slim\Routing\RouteCollectorProxy;


return function (App $app) {

    $app->group('/users', function(RouteCollectorProxy $group) {
        $group->get('/{id}', [UserController::class, 'checkUser']);
        $group->post('', [UserController::class, 'create']);
        $group->post('/{id}/address', [AddressController::class, 'create']);
        $group->post('/{id}/payment', [PaymentController::class, 'create']);
    });

};
