<?php

date_default_timezone_set('America/Sao_Paulo');

$settings = [];

$settings['root'] = dirname(__DIR__);

$settings['error'] = [
    'display_error_details' => false,
    'log_errors' => true,
    'log_error_details' => true,
];

$settings['db'] = [
    'driver' => 'mariadb',
    'host' => 'mariadb',
    'username' => 'root',
    'database' => 'challenge',
    'password' => 'admin',
    'charset' => 'utf8mb4',
    'collation' => 'utf8mb4_unicode_ci',
    'flags' => [
        PDO::ATTR_PERSISTENT => false,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_EMULATE_PREPARES => true,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8mb4 COLLATE utf8mb4_unicode_ci'
    ],
];

$settings['redis'] = [
    'server' => 'tcp://redis:6379?auth=redisadmin',
    'options' => null,
];

$settings['externalPaymentHost'] = 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/';

return $settings;
