<?php

use Challenge\Infrastructure\Http\ErrorHandler;
use Challenge\Infrastructure\Http\Status;
use Selective\BasePath\BasePathMiddleware;
use Slim\App;
use Slim\Middleware\ErrorMiddleware;

return function (App $app) {

    $app->options('/{routes:.+}', function ($request, $response, $args) {
        return $response;
    });
    $app->add(function ($request, $handler) {
      $response = $handler->handle($request);
      $response = $response->withHeader('Access-Control-Allow-Origin', "http://localhost:8080");
      $response = $response->withHeader('Access-Control-Allow-Headers', 'Origin, Content-Type, Authorization, Accept, X-Requested-With, withCredentials');
      $response = $response->withHeader('Access-Control-Allow-Methods', 'GET, POST, HEAD, OPTIONS');

      return $response;
    });

    $app->addBodyParsingMiddleware();

    $app->addRoutingMiddleware();

    $app->add(BasePathMiddleware::class);

    $NotFoundErrorCustomHandler = function () use ($app) {
        $response = $app->getResponseFactory()->createResponse();

        $error = json_encode(ErrorHandler::handle(Status::CODE_404, 'Route Not Found.'));
        $response->getBody()->write($error);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(Status::CODE_404);
    };

    $methodNotAllowedErrorCustomHandler = function () use ($app) {
        $response = $app->getResponseFactory()->createResponse();

        $error = json_encode(ErrorHandler::handle(Status::CODE_405, 'Method Not Allowed.'));
        $response->getBody()->write($error);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(Status::CODE_405);
    };

    $errorMiddleware = $app->addErrorMiddleware(true, true, true);

    $errorMiddleware->setErrorHandler(Slim\Exception\HttpNotFoundException::class, $NotFoundErrorCustomHandler);

    $errorMiddleware->setErrorHandler(Slim\Exception\HttpMethodNotAllowedException::class, $methodNotAllowedErrorCustomHandler);

    $app->add(ErrorMiddleware::class);
};
