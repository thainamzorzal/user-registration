<?php

namespace Challenge\Infrastructure\Repository;

use Exception;

class UserNotFoundException extends Exception
{
    /**
     * @codeCoverageIgnore
     */
    public function __construct($message, $code = 0, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }

}
