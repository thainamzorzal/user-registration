<?php

declare(strict_types=1);

namespace Challenge\Infrastructure\Repository\User;

use Challenge\Domain\User\Entity\UserInterface;
use Challenge\Domain\User\Entity\User;
use Challenge\Domain\User\Repository\UserRepositoryInterface;
use Challenge\Infrastructure\Http\ErrorHandler;
use Challenge\Infrastructure\Http\Status;
use Challenge\Infrastructure\Repository\UserNotFoundException;

class UserRepository implements UserRepositoryInterface
{
    /**
     * @codeCoverageIgnore
     */
    public function  __construct(private \PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param int $id
     *
     * @return UserInterface
     * @throws UserNotFoundException
     */
    public function getById(int $id): UserInterface
    {
        $sql = 'SELECT `id`, `firstName`, `lastName`, `telephone` FROM `user` WHERE `id` = :id';

        $stmt = $this->connection->prepare($sql);
        $stmt->execute([':id' => $id]);

        if (! $stmt->rowCount()) {
            throw new UserNotFoundException(
                ErrorHandler::NOT_FOUND_USER_MESSAGE,
                Status::CODE_404
            );
        }
        $user = new User();
        $user->fill($stmt->fetch());

        return $user;
    }

    /**
     * @param UserInterface $user
     *
     * @return UserInterface
     */
    public function create(UserInterface $user): UserInterface
    {
        $sql = 'INSERT INTO `user` (`id`, `firstName`, `lastName`, `telephone`)
        VALUES (NULL, :firstName, :lastName, :telephone)';

        $stmt = $this->connection->prepare($sql);

        $stmt->execute([
            ':firstName' => $user->getFirstName(),
            ':lastName' => $user->getLastName(),
            ':telephone' => $user->getTelephone(),
        ]);

        $lastInsertId = (int) $this->connection->lastInsertId();

        $user->setId($lastInsertId);

        return $user;
    }
}
