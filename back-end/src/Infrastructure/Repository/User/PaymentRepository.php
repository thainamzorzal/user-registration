<?php

declare(strict_types=1);

namespace Challenge\Infrastructure\Repository\User;

use Challenge\Domain\User\Entity\PaymentInterface;
use Challenge\Domain\User\Repository\PaymentRepositoryInterface;

class PaymentRepository implements PaymentRepositoryInterface
{
    /**
     * @codeCoverageIgnore
     */
    public function  __construct(private \PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param PaymentInterface $payment
     *
     * @return PaymentInterface
     */
    public function create(PaymentInterface $payment): PaymentInterface
    {
        $sql = 'INSERT INTO `user_payment` (`userId`, `accountOwner`, `iban`, `paymentDataId`)
                VALUES (:userId, :accountOwner, :iban, :paymentDataId)';

        $stmt = $this->connection->prepare($sql);

        $stmt->execute([
            ':userId' => $payment->getUserId(),
            ':accountOwner' => $payment->getAccountOwner(),
            ':iban' => $payment->getIban(),
            ':paymentDataId' => $payment->getPaymentDataId(),
        ]);

        return $payment;
    }
}
