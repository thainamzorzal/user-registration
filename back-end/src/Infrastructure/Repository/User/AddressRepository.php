<?php

declare(strict_types=1);

namespace Challenge\Infrastructure\Repository\User;

use Challenge\Domain\User\Entity\AddressInterface;
use Challenge\Domain\User\Entity\UserInterface;
use Challenge\Domain\User\Repository\AddressRepositoryInterface;

class AddressRepository implements AddressRepositoryInterface
{
    /**
     * @codeCoverageIgnore
     */
    public function  __construct(private \PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param UserInterface $user
     * @param AddressInterface $address
     *
     * @return AddressInterface
     */
    public function create(UserInterface $user, AddressInterface $address): AddressInterface
    {
        $sql = 'INSERT INTO `user_address` (`userId`, `street`, `houseNumber`, `zipCode`, `city`)
                VALUES (:userId, :street, :houseNumber, :zipCode, :city)';

        $stmt = $this->connection->prepare($sql);

        $stmt->execute([
            ':userId' => $user->getId(),
            ':street' => $address->getStreet(),
            ':houseNumber' => $address->getHouseNumber(),
            ':zipCode' => $address->getZipCode(),
            ':city' => $address->getCity(),
        ]);

        $address->setUserId($user->getId());

        return $address;
    }
}
