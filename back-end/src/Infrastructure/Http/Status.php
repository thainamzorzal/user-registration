<?php

declare(strict_types=1);

namespace Challenge\Infrastructure\Http;

class Status
{
    const CODE_200 = 200;
    const CODE_201 = 201;
    const CODE_404 = 404;
    const CODE_405 = 405;
    const CODE_422 = 422;
    const CODE_503 = 503;
}
