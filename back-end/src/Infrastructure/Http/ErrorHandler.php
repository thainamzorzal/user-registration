<?php

declare(strict_types=1);

namespace Challenge\Infrastructure\Http;

class ErrorHandler
{
    const DEFAULT_MESSAGE = 'A server error occurred, please try again!';
    const NOT_FOUND_USER_MESSAGE = 'User not found.';

    /**
     * @param int $code
     * @param string $message
     *
     * @return array
     */
    public static function handle(int $code = null, string $message = null): array
    {
        $message = $message ?? self::DEFAULT_MESSAGE;
        $code = $code ?? Status::CODE_503;

        return ['errors' => ['code' => $code, 'message' => $message]];
    }
}
