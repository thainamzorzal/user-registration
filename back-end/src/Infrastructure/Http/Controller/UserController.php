<?php

declare(strict_types=1);

namespace Challenge\Infrastructure\Http\Controller;

use Challenge\Application\Service\UserServiceInterface;
use Challenge\Infrastructure\Http\ErrorHandler;
use Challenge\Infrastructure\Http\Status;
use Challenge\Infrastructure\Repository\UserNotFoundException;
use Exception;
use InvalidArgumentException;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class UserController extends Controller
{
    private UserServiceInterface $userService;

    /**
     * @codeCoverageIgnore
     */
    public function __construct(ContainerInterface  $container)
    {
        $this->userService = $container->get(UserServiceInterface::class);
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     *
     * @return ResponseInterface
     */
    public function create(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        try {

            $fields = $request->getParsedBody();

            $user = $this->userService->create($fields);

            $payload = ['message' => 'User successfully registered!', 'data' => $user];

            return $this->jsonResponse($response, $payload, Status::CODE_201);

        } catch (InvalidArgumentException $e) {
            $error = ErrorHandler::handle(Status::CODE_422, $e->getMessage());
            return $this->jsonResponse($response, $error, Status::CODE_422);

        } catch (Exception $e) {
            $error = ErrorHandler::handle();
            return $this->jsonResponse($response, $error, Status::CODE_503);
        }
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     *
     * @return ResponseInterface
     */
    public function checkUser(
      ServerRequestInterface $request,
      ResponseInterface $response
  ): ResponseInterface {
      try {

          $userId = (int) $this->getPathVariable($request, 'id');

          $user = $this->userService->getById($userId);

          $payload = ['message' => 'User exists!', 'data' => $user];

          return $this->jsonResponse($response, $payload, Status::CODE_200);

      } catch (UserNotFoundException $e) {
          $error = ErrorHandler::handle(Status::CODE_404, $e->getMessage());
          return $this->jsonResponse($response, $error, Status::CODE_404);

      } catch (Exception $e) {
          $error = ErrorHandler::handle();
          return $this->jsonResponse($response, $error, Status::CODE_503);
      }
  }
}
