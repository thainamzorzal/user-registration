<?php

declare(strict_types=1);

namespace Challenge\Infrastructure\Http\Controller;

use Challenge\Application\Service\AddressServiceInterface;
use Challenge\Application\Service\UserServiceInterface;
use Challenge\Infrastructure\Http\ErrorHandler;
use Challenge\Infrastructure\Http\Status;
use Challenge\Infrastructure\Repository\UserNotFoundException;
use Exception;
use InvalidArgumentException;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class AddressController extends Controller
{
    private UserServiceInterface $userService;

    private AddressServiceInterface $addressService;

    /**
     * @codeCoverageIgnore
     */
    public function __construct(ContainerInterface  $container)
    {
        $this->userService = $container->get(UserServiceInterface::class);
        $this->addressService = $container->get(AddressServiceInterface::class);
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     *
     * @return ResponseInterface
     */
    public function create(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        try {

            $userId = (int) $this->getPathVariable($request, 'id');

            $user = $this->userService->getById($userId);

            $addressInfo = $request->getParsedBody();

            $address = $this->addressService->create($user, $addressInfo);

            $payload = ['message' => 'User address successfully registered!', 'data' => $address];

            return $this->jsonResponse($response, $payload, Status::CODE_201);

        } catch (InvalidArgumentException $e) {
            $error = ErrorHandler::handle(Status::CODE_422, $e->getMessage());
            return $this->jsonResponse($response, $error, Status::CODE_422);

        } catch (UserNotFoundException $e) {
            $error = ErrorHandler::handle(Status::CODE_404, $e->getMessage());
            return $this->jsonResponse($response, $error, Status::CODE_404);

        } catch (Exception $e) {
            $error = ErrorHandler::handle();
            return $this->jsonResponse($response, $error, Status::CODE_503);
        }
    }
}
