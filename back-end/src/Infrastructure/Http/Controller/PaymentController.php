<?php

declare(strict_types=1);

namespace Challenge\Infrastructure\Http\Controller;

use Challenge\Application\Service\PaymentServiceInterface;
use Challenge\Application\Service\UserServiceInterface;
use Challenge\Infrastructure\Http\ErrorHandler;
use Challenge\Infrastructure\Http\Status;
use Challenge\Infrastructure\Repository\UserNotFoundException;
use Exception;
use InvalidArgumentException;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class PaymentController extends Controller
{
    private UserServiceInterface $userService;

    private PaymentServiceInterface $paymentService;

    /**
     * @codeCoverageIgnore
     */
    public function __construct(ContainerInterface  $container)
    {
        $this->userService = $container->get(UserServiceInterface::class);
        $this->paymentService = $container->get(PaymentServiceInterface::class);
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     *
     * @return ResponseInterface
     */
    public function create(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        try {

            $userId = (int) $this->getPathVariable($request, 'id');

            $user = $this->userService->getById($userId);

            $paymentInfo = $request->getParsedBody();

            $payment = $this->paymentService->create($user, $paymentInfo);

            $payload = ['message' => 'Payment successfully made!', 'data' => $payment];

            return $this->jsonResponse($response, $payload, Status::CODE_201);

        } catch (InvalidArgumentException $e) {
            $error = ErrorHandler::handle(Status::CODE_422, $e->getMessage());
            return $this->jsonResponse($response, $error, Status::CODE_422);

        } catch (UserNotFoundException $e) {
            $error = ErrorHandler::handle(Status::CODE_404, $e->getMessage());
            return $this->jsonResponse($response, $error, Status::CODE_404);

        } catch (Exception $e) {
            $error = ErrorHandler::handle();
            return $this->jsonResponse($response, $error, Status::CODE_503);
        }
    }
}
