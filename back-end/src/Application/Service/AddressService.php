<?php

declare(strict_types=1);

namespace Challenge\Application\Service;

use Challenge\Domain\User\Entity\Address;
use Challenge\Domain\User\Entity\AddressInterface;
use Challenge\Domain\User\Entity\IAddress;
use Challenge\Domain\User\Entity\UserInterface;
use Challenge\Domain\User\Repository\AddressRepositoryInterface;

class AddressService implements AddressServiceInterface
{
    /**
     * @codeCoverageIgnore
     */
    public function __construct(private AddressRepositoryInterface $repository)
    {
    }

    /**
     * @param UserInterface $user
     * @param array $addressInfo
     *
     * @return IAddress
     */
    public function create(UserInterface $user, array $addressInfo = []): AddressInterface
    {
        $address = new Address();
        $address->fill((object) $addressInfo);

        return $this->repository->create($user, $address);
    }
}
