<?php

declare(strict_types=1);

namespace Challenge\Application\Service;

use Challenge\Domain\User\Entity\User;
use Challenge\Domain\User\Entity\UserInterface;
use Challenge\Domain\User\Repository\UserRepositoryInterface;

class UserService implements UserServiceInterface
{
    /**
     * @codeCoverageIgnore
     */
    public function __construct(private UserRepositoryInterface $repository)
    {
    }

    /**
     * @param int $id
     *
     * @return UserInterface
     */
    public function getById(int $id): UserInterface
    {
        $user = $this->repository->getById($id);

        return $user;
    }

    /**
     * @param array $fields
     *
     * @return UserInterface
     */
    public function create(array $userInfo = []): UserInterface
    {
        $user = new User();
        $user->fill((object) $userInfo);

        $user = $this->repository->create($user);

        return $user;
    }
}
