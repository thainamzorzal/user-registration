<?php

declare(strict_types=1);

namespace Challenge\Application\Service;

use Challenge\Application\Exception\PaymentException;
use Challenge\Domain\User\Entity\PaymentInterface;
use GuzzleHttp\ClientInterface as GuzzleClientInterface;
use GuzzleHttp\Exception\ClientException;
use Psr\Http\Message\ResponseInterface;
use stdClass;

class PaymentExternalService implements PaymentExternalServiceInterface
{
    const PAY_ENDPOINT = 'default/wunderfleet-recruiting-backend-dev-save-payment-data';
    const DEFAULT_ERROR_MESSAGE = 'An error occurred while trying to make payment, please try again.';

    /**
     * @codeCoverageIgnore
     */
    public function __construct(private GuzzleClientInterface $client)
    {
    }

    /**
     * @param PaymentInterface $payment
     *
     * @return PaymentInterface
     * @throws PaymentException
     */
    public function pay(PaymentInterface $payment): PaymentInterface
    {
        try {

            $request = $this->client->request(
              'POST',
              self::PAY_ENDPOINT,
              $this->getPayload($payment)
            );

            $response = $this->checkResponse($request);

            $payment->setPaymentDataId($response->paymentDataId);

            return $payment;

      } catch (ClientException $e) {
        throw new PaymentException(
          self::DEFAULT_ERROR_MESSAGE,
          $e->getResponse()->getStatusCode()
        );
      }
    }

    /**
     * @param PaymentInterface $payment
     *
     * @return array
     */
    private function getPayload($payment): array
    {
        return [
          'json' => [
              'customerId' => $payment->getUserId(),
              'owner' => $payment->getAccountOwner(),
              'iban' => $payment->getIban(),
          ]
      ];
    }

    /**
     * Method to check if the response has a valid JSON string
     *
     * @param ResponseInterface
     *
     * @return stdClass
     * @throws PaymentException
     */
    protected function checkResponse(ResponseInterface $response): stdClass
    {
        $responseDecoded = json_decode($response->getBody()->getContents());

        if (json_last_error() !== JSON_ERROR_NONE) {
          throw new PaymentException(
            self::DEFAULT_ERROR_MESSAGE,
            $responseDecoded->getStatusCode()
          );
        }

        return $responseDecoded;
    }
}
