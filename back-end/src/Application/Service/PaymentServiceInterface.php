<?php

namespace Challenge\Application\Service;

use Challenge\Domain\User\Entity\PaymentInterface;
use Challenge\Domain\User\Entity\UserInterface;

/**
 * @codeCoverageIgnore
 */
interface PaymentServiceInterface
{
    public function create(UserInterface $user, array $paymentInfo = []): PaymentInterface;
}
