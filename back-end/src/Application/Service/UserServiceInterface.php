<?php

namespace Challenge\Application\Service;

use Challenge\Domain\User\Entity\UserInterface;

/**
 * @codeCoverageIgnore
 */
interface UserServiceInterface
{
    public function getById(int $id): UserInterface;
    public function create(array $userInfo = []): UserInterface;
}
