<?php

namespace Challenge\Application\Service;

use Challenge\Domain\User\Entity\AddressInterface;
use Challenge\Domain\User\Entity\UserInterface;

/**
 * @codeCoverageIgnore
 */
interface AddressServiceInterface
{
    public function create(UserInterface $user, array $addressInfo = []): AddressInterface;
}
