<?php

namespace Challenge\Application\Service;

use Challenge\Domain\User\Entity\PaymentInterface;

/**
 * @codeCoverageIgnore
 */
interface PaymentExternalServiceInterface
{
    public function pay(PaymentInterface $payment): PaymentInterface;
}
