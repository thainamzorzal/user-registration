<?php

declare(strict_types=1);

namespace Challenge\Application\Service;

use Challenge\Domain\User\Entity\Payment;
use Challenge\Domain\User\Entity\PaymentInterface;
use Challenge\Domain\User\Entity\UserInterface;
use Challenge\Domain\User\Repository\PaymentRepositoryInterface;

class PaymentService implements PaymentServiceInterface
{
    /**
     * @codeCoverageIgnore
     */
    public function __construct(
      private PaymentRepositoryInterface $repository,
      private PaymentExternalServiceInterface $externalService
    ) {}

    /**
     * @param UserInterface $user
     * @param array $paymentInfo
     *
     * @return PaymentInterface
     */
    public function create(UserInterface $user, array $paymentInfo = []): PaymentInterface
    {

        $payment = new Payment();
        $payment->fill((object) $paymentInfo);
        $payment->setUserId($user->getId());

        $this->externalService->pay($payment);

        return $this->repository->create($payment);
    }
}
