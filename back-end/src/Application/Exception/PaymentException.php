<?php

namespace Challenge\Application\Exception;

use Exception;

class PaymentException extends Exception
{
    /**
     * @codeCoverageIgnore
     */
    public function __construct($message, $code, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }

}
