<?php

declare(strict_types=1);

namespace Challenge\Domain\User\Repository;

use Challenge\Domain\User\Entity\UserInterface;

/**
 * @codeCoverageIgnore
 */
interface UserRepositoryInterface
{
    public function getById(int $id): UserInterface;
    public function create(UserInterface $user): UserInterface;
}
