<?php

declare(strict_types=1);

namespace Challenge\Domain\User\Repository;

use Challenge\Domain\User\Entity\AddressInterface;
use Challenge\Domain\User\Entity\UserInterface;

/**
 * @codeCoverageIgnore
 */
interface AddressRepositoryInterface
{
    public function create(UserInterface $user, AddressInterface $address): AddressInterface;
}
