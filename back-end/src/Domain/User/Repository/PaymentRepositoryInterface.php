<?php

declare(strict_types=1);

namespace Challenge\Domain\User\Repository;

use Challenge\Domain\User\Entity\PaymentInterface;

/**
 * @codeCoverageIgnore
 */
interface PaymentRepositoryInterface
{
    public function create(PaymentInterface $payment): PaymentInterface;
}
