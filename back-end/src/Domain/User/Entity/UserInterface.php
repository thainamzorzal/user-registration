<?php

namespace Challenge\Domain\User\Entity;

use stdClass;

/**
 * @codeCoverageIgnore
 */
interface UserInterface
{
    public function getId(): ?int;
    public function setId(int $id): void;

    public function getFirstName(): string;
    public function setFirstName(null|string $firstName): void;

    public function getLastName(): string;
    public function setLastName(null|string $lastName): void;

    public function getTelephone(): string;
    public function setTelephone(null|string $telephone): void;

    public function fill(stdClass $userInfo): void;
}
