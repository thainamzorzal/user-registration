<?php

declare(strict_types=1);

namespace Challenge\Domain\User\Entity;

use InvalidArgumentException;
use JsonSerializable;
use stdClass;

class Address implements AddressInterface, JsonSerializable
{
    private int $userId;
    private string $street;
    private string $houseNumber;
    private string $zipCode;
    private string $city;

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): void
    {
        if (! $userId) {
            throw new InvalidArgumentException('Invalid User!');
        }
        $this->userId = $userId;
    }

    public function getStreet(): string
    {
        return $this->street;
    }

    public function setStreet(string|null $street): void
    {
        if (empty($street)) {
            throw new InvalidArgumentException('The street cannot be empty!');
        }
        $this->street = $street;
    }

    public function getHouseNumber(): string
    {
        return $this->houseNumber;
    }

    public function setHouseNumber(string|null $houseNumber): void
    {
        if (empty($houseNumber)) {
            throw new InvalidArgumentException('The house number cannot be empty!');
        }
        $this->houseNumber = $houseNumber;
    }

    public function getZipCode(): string
    {
        return $this->zipCode;
    }

    public function setZipCode(string|null $zipCode): void
    {
        if (empty($zipCode)) {
            throw new InvalidArgumentException('The zip code cannot be empty!');
        }

        $this->zipCode = $zipCode;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function setCity(null|string $city): void
    {
        if (empty($city)) {
            throw new InvalidArgumentException('The city cannot be empty!');
        }

        $this->city = $city;
    }

    public function fill(stdClass $addressInfo): void
    {
        if (isset($addressInfo->userId)) {
            $this->setUserId((int) $addressInfo->userId);
        }

        $this->setStreet($addressInfo->street ?? null);
        $this->setHouseNumber($addressInfo->houseNumber ?? null);
        $this->setZipCode($addressInfo->zipCode ?? null);
        $this->setCity($addressInfo->city ?? null);
    }

    public function jsonSerialize(): array
    {
        return [
            'userId' => $this->getUserId(),
            'street' => $this->getStreet(),
            'houseNumber' => $this->getHouseNumber(),
            'zipCode' => $this->getZipCode(),
            'city' => $this->getCity()
        ];
    }
}
