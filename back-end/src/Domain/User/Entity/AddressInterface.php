<?php

namespace Challenge\Domain\User\Entity;

use stdClass;

/**
 * @codeCoverageIgnore
 */
interface AddressInterface
{
    public function getUserId(): int;
    public function setUserId(int $userId): void;

    public function getStreet(): string;
    public function setStreet(null|string $street): void;

    public function getHouseNumber(): string;
    public function setHouseNumber(null|string $houseNumber): void;

    public function getZipCode(): string;
    public function setZipCode(null|string $zipCode): void;

    public function getCity(): string;
    public function setCity(null|string $city): void;

    public function fill(stdClass $addressInfo): void;
}
