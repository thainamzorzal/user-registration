<?php

declare(strict_types=1);

namespace Challenge\Domain\User\Entity;

use InvalidArgumentException;
use JsonSerializable;
use stdClass;

class Payment implements PaymentInterface, JsonSerializable
{
    private int $userId;
    private string $accountOwner;
    private string $iban;
    private null|string $paymentDataId;

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): void
    {
        if (! $userId) {
            throw new InvalidArgumentException('Invalid User!');
        }
        $this->userId = $userId;
    }

    public function getAccountOwner(): string
    {
        return $this->accountOwner;
    }


    public function setAccountOwner(string|null $accountOwner): void
    {
        if (empty($accountOwner)) {
            throw new InvalidArgumentException('The account owner cannot be empty!');
        }
        $this->accountOwner = $accountOwner;
    }

    public function getIban(): string
    {
        return $this->iban;
    }

    public function setIban(string|null $iban): void
    {
        if (empty($iban)) {
            throw new InvalidArgumentException('The IBAN cannot be empty!');
        }
        $this->iban = $iban;
    }

    public function getPaymentDataId(): null|string
    {
        return $this->paymentDataId;
    }

    public function setPaymentDataId(string|null $paymentDataId): void
    {
        $this->paymentDataId = $paymentDataId;
    }

    public function fill(stdClass $paymentInfo): void
    {
        if (isset($paymentInfo->userId)) {
            $this->setUserId((int) $paymentInfo->userId);
        }

        $this->setAccountOwner($paymentInfo->accountOwner ?? null);
        $this->setIban($paymentInfo->iban ?? null);
        $this->setPaymentDataId($paymentInfo->paymentDataId ?? null);
    }

    public function jsonSerialize(): array
    {
        return [
            'userId' => $this->getUserId(),
            'accountOwner' => $this->getAccountOwner(),
            'iban' => $this->getIban(),
            'paymentDataId' => $this->getPaymentDataId()
        ];
    }
}
