<?php

declare(strict_types=1);

namespace Challenge\Domain\User\Entity;

use InvalidArgumentException;
use JsonSerializable;
use stdClass;

class User implements UserInterface, JsonSerializable
{
    private int $id;
    private string $firstName;
    private string $lastName;
    private string $telephone;

    /**
     * @codeCoverageIgnore
     */
    public function __construct(int $id = null)
    {
        if ($id) {
            $this->setId($id);
        }
    }

    public function getId(): ?int
    {
        return $this->id ?? null;
    }

    public function setId(int $id): void
    {
        if (! $id) {
            throw new InvalidArgumentException('Invalid ID!');
        }
        $this->id = $id;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setFirstName(string|null $firstName): void
    {
        if (empty($firstName)) {
            throw new InvalidArgumentException('The first name cannot be empty!');
        }
        $this->firstName = $firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function setLastName(string|null $lastName): void
    {
        if (empty($lastName)) {
            throw new InvalidArgumentException('The last name cannot be empty!');
        }
        $this->lastName = $lastName;
    }

    public function getTelephone(): string
    {
        return $this->telephone;
    }

    public function setTelephone(string|null $telephone): void
    {
        if (empty($telephone)) {
            throw new InvalidArgumentException('The phone number cannot be empty!');
        }

        $this->telephone = $telephone;
    }

    public function fill(stdClass $userInfo): void
    {
        if (isset($userInfo->id)) {
            $this->setId((int) $userInfo->id);
        }

        $this->setFirstName($userInfo->firstName ?? null);
        $this->setLastName($userInfo->lastName ?? null);
        $this->setTelephone($userInfo->telephone ?? null);
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'firstName' => $this->getFirstName(),
            'lastName' => $this->getLastName(),
            'telephone' => $this->getTelephone()
        ];
    }
}
