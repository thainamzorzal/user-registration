<?php

namespace Challenge\Domain\User\Entity;

use stdClass;

/**
 * @codeCoverageIgnore
 */
interface PaymentInterface
{
    public function getUserId(): int;
    public function setUserId(int $userId): void;

    public function getAccountOwner(): string;
    public function setAccountOwner(null|string $accountOwner): void;

    public function getIban(): string;
    public function setIban(null|string $iban): void;

    public function getPaymentDataId(): null|string;
    public function setPaymentDataId(null|string $paymentDataId): void;

    public function fill(stdClass $paymentInfo): void;
}
