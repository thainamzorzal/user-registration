# User Registration Challenge

## Important
The next steps need ```make {command}``` to work, if you are not familiar with this technology, please visit the website [HERE](https://makefile.site/ ).

### Docs:

The API docs can be found in [Swagger](https://app.swaggerhub.com/apis/thainam/user-registration/1.0.0)

The Database ER Diagram can be found in: 

```./back-end/docs/Database Model/```

### Setup:
1 - Clone the repository.

2 - Use the "make" command to setup the project:

```make setup```

```Go get a cup of coffe while the project is built, it can take a while! ☕```

### Testing
Use the "make" command to run tests:

```make test```

If you want to generate the coverage again, use the command below:

```make coverage```

You can access the coverage dashboard at:

```./back-end/coverage/index.html```

### Accessing the application 

After the setup, open your browser and type this URL:

```http://localhost:8080```

That is it, enjoy the application! 🎮

### Possible performance optimizations for my code:

- I could implement some cache (like Redis) in the backend to optimize the response.
- To Apply some tests in the front-end would be very nice (but I'm not very familiar with it yet).
- To apply monitoring (like New Relic) to detect any instability from the back-end.
- As the application grows, it's nice to implement HATEOAS to facilitate the develop.

### Which things I could be done better, than I’ve done it?
- I stored the user data in local storage because is a simple application, but I would use security cookies (SSL) in production, for example.
- I could implement CSRF token and/or captcha to increase security.
- I could implement backend validation.
- Where I use the PDO class (located in the back-end at src/Infrastructure/Repository), I could apply inverse dependecy but I forgot to refactor it!😅

### License
MIT
**Free Software, Yeeeah!**