import { Payment } from './user-payment';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from './user';
import { Address } from './user-address';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json; charset=utf-8',
  })
};

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  userId: null|string = null;

  checkUser() {
    this.userId = localStorage.getItem('userId');
    return this.http.get<any>(`http://api.localhost/users/${this.userId}`);
  }

  addUserAddress(address: Address): Observable<any> {
    this.userId = localStorage.getItem('userId');
    return this.http.post<any>(`http://api.localhost/users/${this.userId}/address`, address, httpOptions);
  }

  addUserPayment(payment: Payment): Observable<any> {
    this.userId = localStorage.getItem('userId')
    return this.http.post<any>(`http://api.localhost/users/${this.userId}/payment`, payment, httpOptions);
  }

  addUser(user: User): Observable<any> {
    return this.http.post<any>('http://api.localhost/users', user, httpOptions);
  }

}
