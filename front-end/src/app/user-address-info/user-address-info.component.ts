import { UserService } from './../user.service';
import { Address } from './../user-address';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-address-info',
  templateUrl: './user-address-info.component.html',
  styleUrls: ['./user-address-info.component.scss']
})
export class UserAddressInfoComponent implements OnInit {

  constructor(public userService: UserService, public route: Router) { }

  address = new Address('', '', '', '');;
  userStep: null|string = null;
  loading = false;
  errorMessage = '';

  ngOnInit(): void {
    this.loading = false;
    this.userService.checkUser().subscribe({
      error: (err) => {
        if (err.status == 404) {
          localStorage.clear();
          this.route.navigate(['/user-personal']);
        }
      }
    });
  }

  onSubmit() {
    this.loading = true;
    this.errorMessage = '';
    this.userService.addUserAddress(this.address).subscribe({
      next: () => {
        localStorage.setItem('userStep', '/user-payment');
        this.route.navigate(['/user-payment'])
      },
      error: (err) => {
        this.loading = false;
        this.errorMessage = 'Server error, please try again.';
      }
    });
  }
}
