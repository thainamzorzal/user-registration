import { Router } from '@angular/router';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'Challenge';
  userId: null|string = null;
  userStep: null|string = null;

  constructor(private route: Router){}

  ngOnInit(): void {
    this.userId = localStorage.getItem('userId');
    if (! this.userId) {
      localStorage.clear();
      this.route.navigate(['/user-personal'])
    }
    this.userStep = localStorage.getItem('userStep');
    if (this.userStep != '' && this.userStep != null) {
      this.route.navigate([this.userStep])
    }
  }
}


