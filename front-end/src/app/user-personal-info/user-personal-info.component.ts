import { UserService } from './../user.service';
import { User } from './../user';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-personal-info',
  templateUrl: './user-personal-info.component.html',
  styleUrls: ['./user-personal-info.component.scss']
})

export class UserPersonalInfoComponent implements OnInit {

  constructor(public userService: UserService, public route: Router) { }

  user = new User('', '', '');
  loading = false;
  errorMessage = '';

  ngOnInit(): void {
    this.loading = false
  }

  onSubmit() {
    this.loading = true;
    this.errorMessage = '';
    this.userService.addUser(this.user).subscribe({
      next: (response) => {
        localStorage.setItem('userId', response.data.id);
        localStorage.setItem('userStep', '/user-address');
        this.route.navigate(['/user-address'])
      },
      error: (err) => {
        this.loading = false;
        this.errorMessage = 'Server error, please try again.';
      }
    });
  }

}
