import { UserPaymentInfoComponent } from './user-payment-info/user-payment-info.component';
import { UserAddressInfoComponent } from './user-address-info/user-address-info.component';
import { UserPersonalInfoComponent } from './user-personal-info/user-personal-info.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'user-personal', component: UserPersonalInfoComponent },
  { path: 'user-address', component: UserAddressInfoComponent },
  { path: 'user-payment', component: UserPaymentInfoComponent },
  { path: '',   redirectTo: '/user-personal', pathMatch: 'full' },
  { path: '**', redirectTo: '/user-personal'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
