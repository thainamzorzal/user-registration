import { UserService } from './../user.service';
import { Payment } from './../user-payment';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-payment-info',
  templateUrl: './user-payment-info.component.html',
  styleUrls: ['./user-payment-info.component.scss']
})
export class UserPaymentInfoComponent implements OnInit {

  constructor(private userService: UserService, public route: Router) { }

  payment = new Payment('', '', '');
  loading = false;
  errorMessage = '';
  successMessage = '';

  ngOnInit(): void {
    this.loading = false;
    this.payment.paymentDataId = localStorage.getItem('paymentDataId')
    this.userService.checkUser().subscribe({
      error: (err) => {
        if (err.status == 404) {
          localStorage.clear();
          this.route.navigate(['/user-personal']);
        }
      }
    });
  }

  onSubmit() {
    this.loading = true
    this.errorMessage = '';
    this.userService.addUserPayment(this.payment).subscribe({
      next: (response) => {
        this.loading = false;
        this.payment.paymentDataId = response.data.paymentDataId;
        localStorage.setItem('paymentDataId', this.payment.paymentDataId);
        this.successMessage = 'Payment successfully made!';
      },
      error: (err) => {
        this.loading = false;
        this.errorMessage = 'Server error, please try again.';
      }
    });
  }
}
