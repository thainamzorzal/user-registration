export class Payment {
  constructor(
    public accountOwner: string,
    public iban: string,
    public paymentDataId: any,
  ) {  }
}
