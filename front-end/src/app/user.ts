export class User {

  constructor(
    public firstName: string,
    public lastName: string,
    public telephone: string,
    public id?: number,
  ) {  }

}
