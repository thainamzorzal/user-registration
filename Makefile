
setup:
	docker-compose up -d --build --force-recreate
	docker exec api composer install
	docker exec frontend npm install
	docker exec frontend ng build --optimization
	docker restart nginx

up:
	docker-compose up -d

down:
	docker-compose down

test:
	docker exec api	./vendor/bin/phpunit --testdox --do-not-cache-result

coverage:
	docker exec api	./vendor/bin/phpunit --coverage-html ./coverage


